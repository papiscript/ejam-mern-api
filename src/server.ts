import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as helmet from 'helmet';
import { getConnectionStatus, startMongoose } from './respository/mongoose';
import { DeploymentModel } from './models/deployment';
import { getConfig } from './config';

const config = getConfig();

const app = express();
startMongoose();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(helmet());

app.get('/api/health', async (req, res) => {
  res.json({
    mongo: getConnectionStatus(),
  });
});

app.get('/api/deployments', async (req, res) => {
  const deployments = await DeploymentModel.find().sort({ _id: -1 }).exec();

  console.info(deployments);

  res.send(deployments);
});

app.post('/api/deployments', async (req, res) => {
  const deployment = await DeploymentModel.create({
    ...req.body,
    deployedAt: new Date().toISOString(),
  });

  console.info(deployment);

  res.send(deployment);
});

app.delete('/api/deployments/:id', async (req, res) => {
  const deleted = await DeploymentModel.deleteOne({ _id: req.params.id });

  console.info(deleted);

  if (deleted.deletedCount !== 1) {
    res.status(400).send({ status: 'failure' });
    return;
  }

  res.send({ id: req.params.id });
});

app.listen(config.port, () => {
  console.info(`App is listening on port ${config.port}`);
});

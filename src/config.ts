import * as dotenv from 'dotenv';

dotenv.config();

export const getConfig = () => {
  const rawPort = process.env.PORT;
  if (!rawPort) {
    console.error('env variable PORT not specified!');

    process.exit(-1);
  }
  const port = +rawPort;
  if (isNaN(port) || port < 1) {
    console.error('Invalid PORT specified!');

    process.exit(-1);
  }

  const dbUser = process.env.MONGO_USER;
  if (!dbUser) {
    console.error('env variable MONGO_USER not specified!');

    process.exit(-1);
  }

  const dbPass = process.env.MONGO_PASS;
  if (!dbPass) {
    console.error('env variable MONGO_PASS not specified!');

    process.exit(-1);
  }

  const dbName = process.env.MONGO_DB_NAME;
  if (!dbName) {
    console.error('env variable MONGO_DB_NAME not specified!');

    process.exit(-1);
  }

  return {
    port,
    dbUser,
    dbPass,
    dbName
  }
}
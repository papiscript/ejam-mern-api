import { prop, getModelForClass } from '@typegoose/typegoose';

class Deployment {
  @prop()
  public url?: string;

  @prop()
  public templateName?: string;

  @prop()
  public version?: string;

  @prop()
  public deployedAt?: string;
}

export const DeploymentModel = getModelForClass(Deployment);
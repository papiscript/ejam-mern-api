import * as mongoose from 'mongoose';
import { getConfig } from '../config';

const getConnectionString = () => {
  const { dbUser, dbPass, dbName } = getConfig();

  return `mongodb+srv://${dbUser}:${dbPass}@ejam-mern.lg1zm.mongodb.net/${dbName}?retryWrites=true&w=majority`;
};

export const startMongoose = () => {
  mongoose.connect(getConnectionString(), { useNewUrlParser: true });

  const db = mongoose.connection;

  db.on('error', console.error.bind(console, 'MongoDB connection error:'));
};

export const getConnectionStatus = () =>
  mongoose.connection.states[mongoose.connection.readyState];
